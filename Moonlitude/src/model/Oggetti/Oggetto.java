package model.Oggetti;

public interface Oggetto {
    
    /**
     * Getter method for the name of the object
     * @return a string value of the name of the object
     */
    public String getNome();  
    
    public String toString();
}
